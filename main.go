package main

import (
	"gopkg.in/alecthomas/kingpin.v2"
	"log"
	"os"
	"strings"
)

var (
	typeJson = "json"
	typeText = "text"
)

var mappingType = map[string]string{
	typeJson: ".json",
	typeText: ".txt",
}

func main() {

	var (
		configFileFrom   = kingpin.Arg("Args args 1 file from", `file dir from example "/var/log/nginx/error.log"`).Required().String()
		configType       = kingpin.Flag("t", "output file default text").Short('t').Default("text").String()
		configFileTarget = kingpin.Flag("o", `output target dir example "/var/log/nginx/error.txt"`).Short('o').Default("").String()
	)
	kingpin.HelpFlag.Short('h')
	kingpin.Parse()

	//validate check file to
	if !strings.HasSuffix(*configFileTarget, ".json") && !strings.HasSuffix(*configFileTarget, ".txt") && *configFileTarget != "" {
		log.Fatal(`missing flag -o format file must be ".txt" or ".json"`)
	}

	if strings.ToLower(*configType) != typeText && strings.ToLower(*configType) != typeJson {
		log.Fatal(`flag missing/wrong -t use a.json.json input "text" or "json"`)
	}

	err := moveFile(*configFileFrom, *configFileTarget, *configType)
	if err != nil {
		log.Fatal(err)
	}
}

func moveFile(from, to, fileType string) (err error) {
	err = os.Chmod(from, 0777)
	if err != nil {
		return
	}

	//Get replace prefix
	if to == "" {
		to = from
	}

	to = to[0:strings.LastIndex(to, ".")] + mappingType[fileType]

	err = os.Rename(from, to)
	if err != nil {
		return
	}
	log.Printf("Success Move file From : %s To %s", from, to)
	return
}
